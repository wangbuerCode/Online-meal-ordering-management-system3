package com.itbaizhan.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DB
{
	private Connection con;

	private PreparedStatement pstm;//PreparedStatement锟斤拷锟斤拷pstm 

	private String user = "root";

	private String password = "root";

	private String className = "com.mysql.cj.jdbc.Driver";//锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟�

	private String url = "jdbc:mysql://localhost:3306/db_dingcan?characterEncoding=utf8&useSSL=false&serverTimezone=UTC&rewriteBatchedStatements=true";
// &amp锟斤拷示&,amp为'',xml转锟斤拷锟�
	public DB()
	{
		try
		{
			Class.forName(className);//锟斤拷锟斤拷JDBC锟斤拷锟斤拷锟斤拷锟斤拷
		} catch (ClassNotFoundException e)
		{
			System.out.println("锟斤拷锟斤拷锟斤拷锟捷匡拷锟斤拷锟斤拷失锟杰ｏ拷");
			e.printStackTrace();
		}
	}

	/** 锟斤拷锟斤拷锟斤拷锟捷匡拷锟斤拷锟斤拷 */
	public Connection getCon()
	{
		try
		{
			con = DriverManager.getConnection(url, user, password);//锟斤拷锟斤拷锟斤拷锟捷匡拷
		} catch (SQLException e)
		{
			System.out.println("锟斤拷锟斤拷锟斤拷锟捷匡拷锟斤拷锟斤拷失锟杰ｏ拷");
			con = null;
			e.printStackTrace();
		}
		return con;
	}
//锟斤拷锟斤拷锟捷匡拷锟斤拷删锟侥的诧拷锟斤拷锟斤拷锟诫，锟斤拷锟斤拷锟斤拷锟�
	public void doPstm(String sql, Object[] params)//params锟角诧拷锟斤拷锟斤拷锟斤拷
	{
		if (sql != null && !sql.equals(""))//要执锟叫碉拷sql锟斤拷洳晃拷锟�
		{
			if (params == null)//锟斤拷锟斤拷锟斤拷锟较诧拷为锟斤拷
				params = new Object[0];//锟斤拷锟斤拷params

			getCon();//锟斤拷锟斤拷getCon()锟斤拷锟斤拷锟斤拷取锟斤拷锟捷匡拷锟斤拷锟斤拷
			if (con != null)
			{
				try
				{
					System.out.println(sql); //锟斤拷锟絊QL锟斤拷锟�
					pstm = con.prepareStatement(sql,
							ResultSet.TYPE_SCROLL_INSENSITIVE,//锟斤拷锟斤拷锟斤拷锟斤拷怨锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷莞谋洳伙拷锟斤拷锟�
							ResultSet.CONCUR_READ_ONLY);//锟斤拷锟斤拷锟街伙拷锟�
					for (int i = 0; i < params.length; i++)
					{
						pstm.setObject(i + 1, params[i]);
					}
					pstm.execute(); //锟矫碉拷锟斤拷锟斤拷锟�
				} catch (SQLException e)
				{
					System.out.println("doPstm()锟斤拷锟斤拷锟斤拷锟斤拷");
					e.printStackTrace();
				}
			}
		}
	}
//锟斤拷取锟斤拷询锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷装
	public ResultSet getRs() throws SQLException
	{
		return pstm.getResultSet();
	}
//锟斤拷取锟斤拷锟铰硷拷录锟侥达拷锟斤拷锟斤拷锟斤拷锟斤拷知锟斤拷锟斤拷锟斤拷锟剿讹拷锟劫硷拷录
	public int getCount() throws SQLException
	{
		return pstm.getUpdateCount();
	}
//锟截憋拷锟斤拷锟捷库，锟酵凤拷占锟矫碉拷锟斤拷源
	public void closed()
	{
		try
		{
			if (pstm != null)
				pstm.close();
		} catch (SQLException e)
		{
			System.out.println("锟截憋拷pstm锟斤拷锟斤拷失锟杰ｏ拷");
			e.printStackTrace();
		}
		try
		{
			if (con != null)
			{
				con.close();
			}
		} catch (SQLException e)
		{
			System.out.println("锟截憋拷con锟斤拷锟斤拷失锟杰ｏ拷");
			e.printStackTrace();
		}
	}
}
